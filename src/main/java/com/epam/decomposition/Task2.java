package src.main.java.com.epam.decomposition;

public class Task2 {
    public static void main(String[] args) {
        String sum1 = calculateSum("9223372036854775807", "9223372036854775807"); // 18 446 744 073 709 551 614
        String sum2 = calculateSum("9223372036854775807", "36854775807"); // 9 223 372 073 709 551 614

        System.out.println(prettifyNumber(sum1));
        System.out.println(prettifyNumber(sum2));
    }

    public static String calculateSum(String x, String y) {
        int xLength = x.length();
        int yLength = y.length();
        int minLength = Math.min(xLength, yLength);

        String result = "";
        int carry = 0;
        for (int i = 0; i < minLength; i++) {
            int sum = Integer.parseInt(x.substring(xLength - 1 - i, xLength - i)) + Integer.parseInt(y.substring(yLength - 1 - i, yLength - i)) + carry;

            carry = sum / 10;
            result = (sum - (carry * 10)) + result;
        }

        if (xLength > yLength) {
            result = (Long.parseLong(x.substring(0, xLength - yLength)) + carry) + result;
        } else if (xLength < yLength) {
            result = (Long.parseLong(y.substring(0, xLength - yLength)) + carry) + result;
        } else if (carry > 0 && xLength == yLength) {
            result = carry + result;
        }

        return result;
    }

    public static String prettifyNumber(String number) {
        String result = "";

        for (int i = 0; i < number.length(); i++) {
            String digit = number.substring(number.length() - 1 - i, number.length() - i);

            if (i % 3 == 0) {
                result = digit + " " + result;
            } else {
                result = digit + result;
            }
        }

        return result;
    }
}
