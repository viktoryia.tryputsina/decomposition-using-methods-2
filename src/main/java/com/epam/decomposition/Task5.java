package src.main.java.com.epam.decomposition;

public class Task5 {
    public static void main(String[] args) {
        int[] array1 = {4, 1, 0, 0, 0, 0, 0, 0, 0, 0};
        int[] array2 = {3, 5, 0, 0, 0, 0, 0, 0, 0, 0};

        createSuperLock(array1);
        createSuperLock(array2);

        printArray(array1);
        printArray(array2);
    }

    public static void createSuperLock(int[] array) {
        for (int i = 2; i < array.length; i++) {
            array[i] = 10 - array[i - 1] - array[i - 2];
        }
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
