package src.main.java.com.epam.decomposition;

public class Task4 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int sumOfConsecutiveNumbers1 = getSumOfConsecutiveNumbers(array, 2, 3);
        int sumOfConsecutiveNumbers2 = getSumOfConsecutiveNumbers(array, 4, 4);
        int sumOfConsecutiveNumbers3 = getSumOfConsecutiveNumbers(array, 7, 1);

        System.out.println(sumOfConsecutiveNumbers1);
        System.out.println(sumOfConsecutiveNumbers2);
        System.out.println(sumOfConsecutiveNumbers3);
    }

    public static int getSumOfConsecutiveNumbers(int[] array, int indexFrom, int countOfElements) {
        int result = 0;

        for (int i = indexFrom; i < indexFrom + countOfElements; i++) {
            result += array[i];
        }

        return result;
    }
}
