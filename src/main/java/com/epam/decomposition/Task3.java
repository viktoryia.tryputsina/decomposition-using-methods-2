package src.main.java.com.epam.decomposition;

import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {
        int[] armstrongNumbers1 = findArmstrongNumbers(1000);
        int[] armstrongNumbers2 = findArmstrongNumbers(1000000);

        printArray(armstrongNumbers1);
        printArray(armstrongNumbers2);
    }

    public static int[] findArmstrongNumbers(int numberTo) {
        int[] result = new int[numberTo];

        int i = 1;
        int j = 0;
        while (i <= numberTo) {
            String numberToString = String.valueOf(i);
            int length = numberToString.length();
            int sumOfPowers = 0;

            for (int k = 0; k < length; k++) {
                int currentDigit = Integer.parseInt(numberToString.substring(k, k + 1));
                double currentDigitPower = Math.pow(currentDigit, length);
                sumOfPowers += currentDigitPower;
            }

            if (sumOfPowers == i) {
                result[j] = sumOfPowers;
                j++;
            }

            i++;
        }

        return Arrays.copyOf(result, j);
    }

    public static void printArray(int[] array) {
        for (int i : array) {
            System.out.print(i + " ");
        }

        System.out.println();
    }
}
