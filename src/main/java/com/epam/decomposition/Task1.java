package src.main.java.com.epam.decomposition;

import java.util.Arrays;

public class Task1 {
    public static void main(String[] args) {
        // I decided to check is given array consists of several consecutive prime numbers.
        // So if we have, for example, three numbers like {7, 23, 41} the method will return false because they are not consecutive.
        int[] values1 = {5, 7, 11};
        int[] values2 = {829, 839, 853};
        int[] values3 = {5, 7, 13};
        int[] values4 = {5, 7, 12};
        int[] values5 = {5, 7, 11, 13}; // also works with bigger arrays :-)
        int[] values6 = {5, 7, 12, 13};

        boolean isCoprime1 = checkIsCoprime(values1);
        boolean isCoprime2 = checkIsCoprime(values2);
        boolean isCoprime3 = checkIsCoprime(values3);
        boolean isCoprime4 = checkIsCoprime(values4);
        boolean isCoprime5 = checkIsCoprime(values5);
        boolean isCoprime6 = checkIsCoprime(values6);

        System.out.println(isCoprime1);
        System.out.println(isCoprime2);
        System.out.println(isCoprime3);
        System.out.println(isCoprime4);
        System.out.println(isCoprime5);
        System.out.println(isCoprime6);
    }

    public static boolean checkIsCoprime(int[] values) {
        Arrays.sort(values);
        int[] primeNumbersInOrder = getPrimeNumbersRange(values);

        boolean isCoprime = true;

        for (int i = 0; i < values.length; i++) {
            if (values[i] != primeNumbersInOrder[i]) {
                isCoprime = false;
                break;
            }
        }

        return isCoprime;
    }

    public static int[] getPrimeNumbersRange(int[] values) {
        int[] results = new int[values.length];
        int j = values[0];
        for (int i = 0; i < values.length; j++) {
            if (checkIsPrime(j)) {
                results[i] = j;
                i++;
            }
        }

        return results;
    }


    public static boolean checkIsPrime(int number) {
        boolean isPrime = true;
        if (number <= 1) {
            isPrime = false;
            return isPrime;
        } else {
            for (int i = 2; i <= number / 2; i++) {
                if ((number % i) == 0) {
                    isPrime = false;
                    break;
                }
            }
            return isPrime;
        }
    }


}
